variable "bucket_id" {
  description = "The S3 bucket name to store the access logs in"
  type        = string
}

variable "certificate_arn" {
  description = "ARN of the certificate to use for HTTPS listener. Required if `enable_https_listener` is set"
  type        = string
  default     = ""
}

variable "custom_tags" {
  description = "A map of tags that will be appended to the tags applied by the module (Name, envtype, envname.). Any tag supplied that matches the name of a tag set by the module will take precedence"
  type        = map(any)
  default     = {}
}

variable "enable_deletion_protection" {
  description = "If true, deletion of the load balancer will be disabled via the AWS API. This will prevent Terraform from deleting the load balancer. Defaults to false"
  type        = bool
  default     = false
}

variable "enable_http_listener" {
  description = "If true, enable default HTTP target group and listener"
  type        = bool
  default     = false
}

variable "enable_https_listener" {
  description = "If true, enable default HTTPS target group and listener"
  type        = bool
  default     = false
}

variable "enable_http_target_group" {
  description = "Flag to enable or disable the default http target group"
  type        = bool
  default     = false
}

variable "enable_https_target_group" {
  description = "Flag to enable or disable the default https target group"
  type        = bool
  default     = false
}

variable "envname" {
  description = "The name of the environment to deploy into"
  type        = string
}

variable "envtype" {
  description = "The type of environment to deploy into, e.g. beta, staging, live"
  type        = string
}

variable "idle_timeout" {
  description = "The time in seconds that the connection is allowed to be idle"
  type        = number
  default     = "60"
}

variable "internal" {
  description = "If true, the ALB will be internal"
  type        = bool
  default     = false
}

variable "listener_ssl_policy" {
  description = "The AWS SSL Policy to assign the HTTPS listener"
  type        = string
  default     = "ELBSecurityPolicy-2015-05"
}

variable "name" {
  description = "The name of the ALB. This name must be unique within your AWS account, can have a maximum of 32 characters, must contain only alphanumeric characters or hyphens, and must not begin or end with a hyphen. If not specified, Terraform will autogenerate a name beginning with tf-lb."
  type        = string
}

variable "service" {
  description = "The name of the service / product you're deploying"
  type        = string
}

variable "security_groups" {
  description = "A list of security group IDs to assign to the LB"
  type        = list(string)
  default     = []
}

variable "subnets" {
  description = "A list of subnet IDs to attach to the LB"
  type        = list(string)
}

variable "target_health_check_path" {
  description = " The destination for the health check request"
  type        = string
  default     = "/"
}

variable "target_health_check_port" {
  description = "The port to use to connect with the target. Valid values are either ports 1-65536, or `traffic-port`"
  type        = string
  default     = "traffic-port"
}

variable "target_port" {
  description = "The port on which targets receive traffic, unless overridden when registering a specific target"
  type        = number
  default     = 80
}

variable "vpc_id" {
  description = "The identifier of the VPC in which to create the target group"
  type        = string
}

variable "waf_enabled" {
  description = "Enable the AWS Web Application Firewall (WAF) in front of the load balancer"
  default     = false
}

variable "webaclid" {
  description = "The ID of a WAF web ACL to attach to use. Required if `waf_enabled` is set"
  type        = string
  default     = ""
}
