---
content: |-
  # Terraform Module: AWS Application Load Balancer

  A Terraform module that can provision an [ALB](https://docs.aws.amazon.com/elasticloadbalancing/latest/application/introduction.html),
  with sub modules to provider [Listeners](https://docs.aws.amazon.com/elasticloadbalancing/latest/application/load-balancer-listeners.html),
  [Listener Rules](https://docs.aws.amazon.com/elasticloadbalancing/latest/application/listener-update-rules.html)
  and [Target Groups](https://docs.aws.amazon.com/elasticloadbalancing/latest/application/load-balancer-target-groups.html).

  * [Example Usage](#example-usage)
    * [Basic](#basic)
  * [Requirements](#requirements)
  * [Inputs](#inputs)
  * [Outputs](#outputs)
  * [Contributing](#contributing)
  * [Change Log](#change-log)
  
  ## Example Usage

  ### Basic

  This is a bare bones public ALB deployment that has no listeners or security
  groups configured. These can be added and managed by individual application
  stacks so you can manage multiple applications through one load balancer.

  ```hcl
  {{ include "examples/basic/main.tf" }}
  ```

  {{ .Requirements }}

  {{ .Inputs }}

  {{ .Outputs }}

  ## Contributing

  Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details on how to contribute to changes to this module.

  ## Change Log

  Please see [CHANGELOG.md](CHANGELOG.md) for changes made between different tag versions of the module.

formatter: markdown

header-from: main.tf

output:
  file: README.md
  mode: replace
  # By default this adds a header and footer comment that shows up Bitbucket
  template: |-
    {{ .Content }}

# Can't, and shouldn't, set what values the outputs are at the module level
output-values:
  enabled: false

# TODO - Ideally this would be true so we can document the local modules
# however it expects them to be under modules/ (or another single path). We
# have teams who are using the // syntax to use the submodules on their own, 
# so changing it will break their deployments. It should be picked up in a
# major version bump
recursive:
  enabled: false

settings:
  anchor: false
  default: true
  description: true
  hide-empty: true
  html: false
  indent: 2
  required: true
  sensitive: false
  type: true

sort:
  by: required
  enabled: true

# Pinned to 0.15.X until this bug with indentation is fixed
# https://github.com/terraform-docs/terraform-docs/issues/577
version: ~> v0.15.0
