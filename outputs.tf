output "alb_arn" {
  description = "*string* The ARN of the ALB"
  value       = aws_alb.alb.arn
}

output "alb_arn_suffix" {
  description = "*string* The ARN suffic of the ALB"
  value       = aws_alb.alb.arn_suffix
}

output "alb_dns_name" {
  description = "The DNS name assigned to the ALB"
  value       = aws_alb.alb.dns_name
}

output "alb_id" {
  description = "*string* The ID of the ALB"
  value       = aws_alb.alb.id
}

output "alb_zone_id" {
  description = "The Route 53 zone the DNS name is in"
  value       = aws_alb.alb.zone_id
}

output "default_target_group_arn" {
  description = "The ARN of the default target group"
  value       = module.http_target_group.alb_target_group_arn
}

output "http_listener_arn" {
  description = "The ARN of the HTTP listener"
  value       = module.http_listener.alb_listener_arn
}

output "https_listener_arn" {
  description = "The ARN of the HTTPS listener"
  value       = module.https_listener.alb_listener_arn
}
