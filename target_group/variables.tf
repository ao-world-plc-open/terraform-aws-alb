# TODO - All the numbers need their type setting correctly to not be a string
# however that could be a breaking change

variable "custom_tags" {
  description = "A map of tags that will be appended to the tags applied by the module (Name, envtype, envname.). Any tag supplied that matches the name of a tag set by the module will take precedence"
  type        = map(any)
  default     = {}
}

variable "deregistration_delay" {
  description = "The amount time for Elastic Load Balancing to wait before changing the state of a deregistering target from draining to unused. The range is 0-3600 seconds"
  type        = string
  default     = "300"
}

variable "envname" {
  description = "The name of the environment to deploy into"
  type        = string
}

variable "envtype" {
  description = "The type of environment to deploy into, e.g. beta, staging, live"
  type        = string
}

variable "health_check_healthy_threshold" {
  description = "The number of consecutive health checks successes required before considering an unhealthy target healthy"
  type        = string
  default     = "5"
}

variable "health_check_matcher" {
  description = "The HTTP codes to use when checking for a successful response from a target. You can specify multiple values (for example, \"200,202\") or a range of values (for example, \"200-299\")"
  type        = string
  default     = "200"
}

variable "health_check_interval" {
  description = "The approximate amount of time, in seconds, between health checks of an individual target. Minimum value 5 seconds, Maximum value 300 seconds"
  type        = string
  default     = "30"
}

variable "health_check_path" {
  description = "The destination for the health check request"
  type        = string
  default     = "/"
}

variable "health_check_port" {
  description = "The port to use to connect with the target. Valid values are either ports 1-65536, or traffic-port. Defaults to traffic-port"
  type        = string
  default     = "80"
}

variable "health_check_protocol" {
  description = "The protocol to use to connect with the target. Defaults to HTTP"
  type        = string
  default     = "HTTP"
}

variable "health_check_timeout" {
  description = "The amount of time, in seconds, during which no response means a failed health check"
  type        = string
  default     = "5"
}

variable "health_check_unhealthy_threshold" {
  description = "The number of consecutive health check failures required before considering the target unhealthy"
  type        = string
  default     = "2"
}

variable "is_enabled" {
  description = "This is used only when module is invoked from the main ALB module"
  type        = bool
  default     = true
}

variable "service" {
  description = "The name of the service / product you're deploying"
  type        = string
}

variable "stickiness" {
  description = "If true, enables stickiness"
  type        = bool
  default     = false
}

variable "stickiness_cookie_duration" {
  description = "The time period, in seconds, during which requests from a client should be routed to the same target. After this time period expires, the load balancer-generated cookie is considered stale. The range is 1 second to 1 week (604800 seconds)"
  type        = string
  default     = "86400"
}

variable "stickiness_type" {
  description = "The type of sticky sessions. The only current possible value is lb_cookie"
  type        = string
  default     = "lb_cookie"
}

variable "target_name" {
  description = "The name of the target group"
  type        = string
}

variable "target_port" {
  description = "The port on which targets receive traffic, unless overridden when registering a specific target"
  type        = string
  default     = "80"
}

variable "target_protocol" {
  description = "The protocol to use for routing traffic to the targets"
  type        = string
  default     = "HTTP"
}

variable "vpc_id" {
  description = "The identifier of the VPC in which to create the target group"
  type        = string
}
