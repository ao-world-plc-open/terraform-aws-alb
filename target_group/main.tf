resource "aws_alb_target_group" "alb_target_group" {
  count = var.is_enabled ? 1 : 0

  deregistration_delay = var.deregistration_delay
  name                 = var.target_name
  port                 = var.target_port
  protocol             = var.target_protocol
  tags = merge({
    Name        = var.target_name,
    Environment = var.envname,
    EnvType     = var.envtype
  }, var.custom_tags)
  vpc_id = var.vpc_id

  health_check {
    healthy_threshold   = var.health_check_healthy_threshold
    interval            = var.health_check_interval
    matcher             = var.health_check_matcher
    path                = var.health_check_path
    port                = var.health_check_port
    protocol            = var.health_check_protocol
    timeout             = var.health_check_timeout
    unhealthy_threshold = var.health_check_unhealthy_threshold
  }

  lifecycle {
    create_before_destroy = true
  }

  stickiness {
    cookie_duration = var.stickiness_cookie_duration
    enabled         = var.stickiness
    type            = var.stickiness_type
  }
}
