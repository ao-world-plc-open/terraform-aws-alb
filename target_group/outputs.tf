output "alb_target_group_arn" {
  description = "*string* The ARN of the ALBs target group"
  value       = element(concat(aws_alb_target_group.alb_target_group.*.arn, []), 0)
}

output "alb_target_group_arn_suffix" {
  description = "*string* The ARN suffix of the ALBs target group"
  value       = element(concat(aws_alb_target_group.alb_target_group.*.arn_suffix, []), 0)
}

output "alb_target_group_id" {
  description = "*string* The ID of the ALBs target group"
  value       = element(concat(aws_alb_target_group.alb_target_group.*.id, []), 0)
}

output "alb_target_group_name" {
  description = "*string* The name of the ALBs target group"
  value       = element(concat(aws_alb_target_group.alb_target_group.*.name, []), 0)
}
