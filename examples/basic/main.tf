module "alb" {
  source = "git::https://gitlab.com/ao-world-plc-open/terraform-aws-alb.git?ref=v1.1.0"

  bucket_id = "bucket-id"
  envname   = "devops"
  envtype   = "demo"
  name      = "devops-demo-website-alb"
  service   = "website"
  subnets = [
    "subnet-id1",
    "subnet-id2",
    "subnet-id3"
  ]
  vpc_id = "vpc-3943209438709"
}