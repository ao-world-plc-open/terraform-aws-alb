package test

import (
	"os"
	"path"
	"testing"

	"github.com/gruntwork-io/terratest/modules/terraform"
	terraformCore "github.com/hashicorp/terraform/terraform"
)

func TestUT_SingleALB(t *testing.T) {
	t.Parallel()

	// Test case for ALB Name
	testCases := map[string]string{
		"TestALBName": "TestALBName",
		// "ALLCAPS":     "TESTALBNAME",
		// "s_p-e(c)i.a_l": "specialdata001",
	}

	for input, expected := range testCases {
		// Specify the test case folder and "-var" options
		tfOptions := &terraform.Options{
			TerraformDir: "./fixtures/alb-only",
			Vars: map[string]interface{}{
				"name": input,
				"custom_tags": map[string]string{
					"foo": "bar",
					"zig": "zag",
				},
			},
		}

		// Terraform init and plan only
		tfPlanOutput := "terraform.tfplan"
		terraform.Init(t, tfOptions)
		terraform.RunTerraformCommand(t, tfOptions, terraform.FormatArgs(tfOptions, "plan", "-out="+tfPlanOutput)...)

		// Read and Parse the plan output
		f, err := os.Open(path.Join(tfOptions.TerraformDir, tfPlanOutput))
		if err != nil {
			t.Fatal(err)
		}
		defer f.Close()
		plan, err := terraformCore.ReadPlan(f)
		if err != nil {
			t.Fatal(err)
		}

		// validate the test result
		for _, mod := range plan.Diff.Modules {
			if len(mod.Path) == 2 && mod.Path[0] == "root" && mod.Path[1] == "alb" {
				actual := mod.Resources["aws_alb.alb"].Attributes["name"].New
				if actual != expected {
					t.Fatalf("Expect %v, but found %v", expected, actual)
				}

				expected = "5"
				actual = mod.Resources["aws_alb.alb"].Attributes["tags.%"].New
				if actual != expected {
					t.Fatalf("Expect %v, but found %v", expected, actual)
				}

				expected = "bar"
				actual = mod.Resources["aws_alb.alb"].Attributes["tags.foo"].New
				if actual != expected {
					t.Fatalf("Expect %v, but found %v", expected, actual)
				}
			}
		}
	}
}
