variable "name" {}

variable "custom_tags" {
  type = map(string)
}


module "target_group" {
  source = "../../../target_group"

  envname     = "terratest"
  envtype     = "test"
  service     = "website"
  target_name = var.name
  vpc_id      = "vpc-3943209438709"
  custom_tags = var.custom_tags
}

provider "aws" {
  region = "eu-west-1"
}

