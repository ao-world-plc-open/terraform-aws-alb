variable "name" {}

variable "custom_tags" {
  type = map(string)
}


module "alb" {
  source = "../../../"

  envname              = "terratest"
  envtype              = "test"
  service              = "website"
  name                 = var.name
  enable_http_listener = true
  security_groups      = ["sg-8457489540"]
  subnets              = ["subnetid-245454656"]
  vpc_id               = "vpc-3943209438709"
  custom_tags          = var.custom_tags
  bucket_id            = "some_bucket"
}

resource "aws_security_group" "test" {
  name = "alb-test"
}

provider "aws" {
  region = "eu-west-1"
}

