# Terraform Module: AWS Application Load Balancer

A Terraform module that can provision an [ALB](https://docs.aws.amazon.com/elasticloadbalancing/latest/application/introduction.html),
with sub modules to provider [Listeners](https://docs.aws.amazon.com/elasticloadbalancing/latest/application/load-balancer-listeners.html),
[Listener Rules](https://docs.aws.amazon.com/elasticloadbalancing/latest/application/listener-update-rules.html)
and [Target Groups](https://docs.aws.amazon.com/elasticloadbalancing/latest/application/load-balancer-target-groups.html).

* [Example Usage](#example-usage)
  * [Basic](#basic)
* [Requirements](#requirements)
* [Inputs](#inputs)
* [Outputs](#outputs)
* [Contributing](#contributing)
* [Change Log](#change-log)

## Example Usage

### Basic

This is a bare bones public ALB deployment that has no listeners or security
groups configured. These can be added and managed by individual application
stacks so you can manage multiple applications through one load balancer.

```hcl
module "alb" {
  source = "git::https://gitlab.com/ao-world-plc-open/terraform-aws-alb.git?ref=v1.1.0"

  bucket_id = "bucket-id"
  envname   = "devops"
  envtype   = "demo"
  name      = "devops-demo-website-alb"
  service   = "website"
  subnets = [
    "subnet-id1",
    "subnet-id2",
    "subnet-id3"
  ]
  vpc_id = "vpc-3943209438709"
}
```

## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| aws | >= 3.26.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| bucket\_id | The S3 bucket name to store the access logs in | `string` | n/a | yes |
| envname | The name of the environment to deploy into | `string` | n/a | yes |
| envtype | The type of environment to deploy into, e.g. beta, staging, live | `string` | n/a | yes |
| name | The name of the ALB. This name must be unique within your AWS account, can have a maximum of 32 characters, must contain only alphanumeric characters or hyphens, and must not begin or end with a hyphen. If not specified, Terraform will autogenerate a name beginning with tf-lb. | `string` | n/a | yes |
| service | The name of the service / product you're deploying | `string` | n/a | yes |
| subnets | A list of subnet IDs to attach to the LB | `list(string)` | n/a | yes |
| vpc\_id | The identifier of the VPC in which to create the target group | `string` | n/a | yes |
| certificate\_arn | ARN of the certificate to use for HTTPS listener. Required if `enable_https_listener` is set | `string` | `""` | no |
| custom\_tags | A map of tags that will be appended to the tags applied by the module (Name, envtype, envname.). Any tag supplied that matches the name of a tag set by the module will take precedence | `map(any)` | `{}` | no |
| enable\_deletion\_protection | If true, deletion of the load balancer will be disabled via the AWS API. This will prevent Terraform from deleting the load balancer. Defaults to false | `bool` | `false` | no |
| enable\_http\_listener | If true, enable default HTTP target group and listener | `bool` | `false` | no |
| enable\_http\_target\_group | Flag to enable or disable the default http target group | `bool` | `false` | no |
| enable\_https\_listener | If true, enable default HTTPS target group and listener | `bool` | `false` | no |
| enable\_https\_target\_group | Flag to enable or disable the default https target group | `bool` | `false` | no |
| idle\_timeout | The time in seconds that the connection is allowed to be idle | `number` | `"60"` | no |
| internal | If true, the ALB will be internal | `bool` | `false` | no |
| listener\_ssl\_policy | The AWS SSL Policy to assign the HTTPS listener | `string` | `"ELBSecurityPolicy-2015-05"` | no |
| security\_groups | A list of security group IDs to assign to the LB | `list(string)` | `[]` | no |
| target\_health\_check\_path | The destination for the health check request | `string` | `"/"` | no |
| target\_health\_check\_port | The port to use to connect with the target. Valid values are either ports 1-65536, or `traffic-port` | `string` | `"traffic-port"` | no |
| target\_port | The port on which targets receive traffic, unless overridden when registering a specific target | `number` | `80` | no |
| waf\_enabled | Enable the AWS Web Application Firewall (WAF) in front of the load balancer | `bool` | `false` | no |
| webaclid | The ID of a WAF web ACL to attach to use. Required if `waf_enabled` is set | `string` | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| alb\_arn | *string* The ARN of the ALB |
| alb\_arn\_suffix | *string* The ARN suffic of the ALB |
| alb\_dns\_name | The DNS name assigned to the ALB |
| alb\_id | *string* The ID of the ALB |
| alb\_zone\_id | The Route 53 zone the DNS name is in |
| default\_target\_group\_arn | The ARN of the default target group |
| http\_listener\_arn | The ARN of the HTTP listener |
| https\_listener\_arn | The ARN of the HTTPS listener |

## Contributing

Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details on how to contribute to changes to this module.

## Change Log

Please see [CHANGELOG.md](CHANGELOG.md) for changes made between different tag versions of the module.