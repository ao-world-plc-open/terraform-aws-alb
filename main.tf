resource "aws_alb" "alb" {
  enable_deletion_protection = var.enable_deletion_protection
  idle_timeout               = var.idle_timeout
  internal                   = var.internal
  name                       = var.name
  security_groups            = var.security_groups
  subnets                    = var.subnets

  access_logs {
    bucket  = var.bucket_id
    enabled = true
    prefix  = var.service
  }

  tags = merge({
    Environment = var.envname,
    EnvType     = var.envtype,
    Name        = var.name
  }, var.custom_tags)
}

resource "aws_cloudformation_stack" "Waf_association" {
  count = var.waf_enabled ? 1 : 0

  name = "${var.name}-alb-waf-association"
  parameters = {
    albname  = "test"
    albarn   = aws_alb.alb.id
    webaclid = var.webaclid
  }
  template_body = <<STACK
{
        "Parameters": {
            "albname": {
            "Type": "String",
            "Default": "ALB Name"
            },
            "albarn": {
            "Type": "String",
            "Default": "ALB Arn"
            },
            "webaclid": {
            "Type": "String",
            "Default": "Waf Web Acl ID"
            }
        },
        "Resources": {
            
            "WebACLAssociation": {
            "Type" : "AWS::WAFRegional::WebACLAssociation",
            "Properties" : {          
            "ResourceArn" : {"Ref" : "albarn"},
            "WebACLId" : {"Ref" : "webaclid"}
            }
        }
        }
}
STACK
}

module "http_target_group" {
  source = "./target_group"

  custom_tags       = var.custom_tags
  envname           = var.envname
  envtype           = var.envtype
  health_check_path = var.target_health_check_path
  health_check_port = var.target_health_check_port
  is_enabled        = var.enable_http_target_group
  service           = var.service
  target_name       = "${var.envname}-${var.service}-http-tg"
  target_port       = var.target_port
  vpc_id            = var.vpc_id
}

module "https_target_group" {
  source = "./target_group"

  custom_tags       = var.custom_tags
  envname           = var.envname
  envtype           = var.envtype
  health_check_path = var.target_health_check_path
  health_check_port = var.target_health_check_port
  is_enabled        = var.enable_https_target_group
  service           = var.service
  target_name       = "${var.envname}-${var.service}-https-tg"
  target_port       = var.target_port
  vpc_id            = var.vpc_id
}

module "http_listener" {
  source = "./listener"

  enable_http_target_group = var.enable_http_target_group
  is_enabled               = var.enable_http_listener
  load_balancer_arn        = aws_alb.alb.arn
  target_group_arn         = module.http_target_group.alb_target_group_arn
}

module "https_listener" {
  source = "./listener"

  enable_http_target_group = var.enable_https_target_group
  is_enabled               = var.enable_https_listener
  listener_certificate_arn = var.certificate_arn
  listener_port            = "443"
  listener_protocol        = "HTTPS"
  listener_ssl_policy      = var.listener_ssl_policy
  load_balancer_arn        = aws_alb.alb.arn
  target_group_arn         = module.https_target_group.alb_target_group_arn
}
