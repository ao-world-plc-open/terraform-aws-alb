
output "alb_listener_arn" {
  description = "*string* The ARN of the listener"
  value       = element(concat(aws_alb_listener.alb_listener_to_target_group.*.arn, aws_alb_listener.alb_listener_to_redirect.*.id, []), 0)
}

output "alb_listener_id" {
  description = "*string* The ID of the listener"
  value       = element(concat(aws_alb_listener.alb_listener_to_target_group.*.id, aws_alb_listener.alb_listener_to_redirect.*.id, []), 0)
}
