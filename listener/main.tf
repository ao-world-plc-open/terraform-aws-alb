locals {
  enabled_redirect     = var.is_enabled == 1 ? var.enable_http_target_group ? 0 : 1 : 0
  enabled_target_group = var.is_enabled == 1 ? var.enable_http_target_group ? 1 : 0 : 0
}

resource "aws_alb_listener" "alb_listener_to_target_group" {
  count = local.enabled_target_group

  certificate_arn   = var.listener_certificate_arn
  load_balancer_arn = var.load_balancer_arn
  port              = var.listener_port
  protocol          = var.listener_protocol
  ssl_policy        = var.listener_certificate_arn != "" ? var.listener_ssl_policy : ""

  default_action {
    target_group_arn = var.target_group_arn
    type             = var.listener_action_type
  }
}

resource "aws_alb_listener" "alb_listener_to_redirect" {
  count = local.enabled_redirect

  certificate_arn   = var.listener_certificate_arn
  load_balancer_arn = var.load_balancer_arn
  port              = var.listener_port
  protocol          = var.listener_protocol
  ssl_policy        = var.listener_certificate_arn != "" ? var.listener_ssl_policy : ""

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "Requires host header in request"
      status_code  = "500"
    }
  }
}

resource "aws_alb_listener_rule" "redirect_http_to_https" {
  count = var.http_to_https_enabled

  listener_arn = aws_alb_listener.alb_listener_to_target_group[count.index].arn

  action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }


  condition {

    http_header {
      http_header_name = "X-Forwarded-For"
      values           = var.hostheaders
    }
  }
}
