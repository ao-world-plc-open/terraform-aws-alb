variable "enable_http_target_group" {
  description = "If the desire is for the default to action for the listener is to forward traffic this should be true, else a HTTP response rule wil be created"
  type        = bool
  default     = true
}

variable "hostheaders" {
  description = "host headers to redirect http to HTTPS"
  type        = list(string)
  default     = ["somesite.ao.com"]
}

variable "http_to_https_enabled" {
  description = "Determines if this listener will 301 redirect http to HTTPS"
  type        = string
  default     = 0
}

variable "is_enabled" {
  description = "This is used only when module is invoked from the main ALB module"
  type        = bool
  default     = true
}

variable "load_balancer_arn" {
  description = "The ARN of the load balancer."
  type        = string
}

variable "listener_action_type" {
  description = "The type of routing action. The only valid value is forward."
  type        = string
  default     = "forward"
}

variable "listener_certificate_arn" {
  description = "The ARN of the SSL server certificate. Exactly one certificate is required if the protocol is HTTPS."
  type        = string
  default     = ""
}

# TODO - This should be a number
variable "listener_port" {
  description = "The port on which the load balancer is listening."
  type        = string
  default     = "80"
}

variable "listener_protocol" {
  description = "The protocol for connections from clients to the load balancer. Valid values are HTTP and HTTPS. Defaults to HTTP."
  type        = string
  default     = "HTTP"
}

variable "listener_ssl_policy" {
  description = "The name of the SSL Policy for the listener. Required if protocol is HTTPS. The only valid value is currently ELBSecurityPolicy-2015-05."
  type        = string
  default     = "ELBSecurityPolicy-2015-05"
}

variable "target_group_arn" {
  description = "The ARN of the Target Group to which to route traffic."
  type        = string
}
