output "alb_listener_arn" {
  description = "*string* The ARN of the listener rule"
  value       = element(concat(aws_alb_listener_rule.listener_rule.*.arn, []), 0)
}

output "alb_listener_id" {
  description = "*string* The ID of the listener rule"
  value       = element(concat(aws_alb_listener_rule.listener_rule.*.id, []), 0)
}
