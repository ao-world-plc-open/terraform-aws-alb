variable "condition_type" {
  description = "The condition type for the listener rule. set to path-pattern or host-header"
  type        = string
}

variable "condition_value" {
  description = "The condition type for the listener value"
  type        = string
}

variable "load_balancer_arn" {
  description = "The ARN of the load balancer"
  type        = string
}

variable "priority" {
  description = "The priority for the listener rule"
  type        = number
}

variable "target_group_arn" {
  description = "The ARN of the target group"
  type        = string
}
