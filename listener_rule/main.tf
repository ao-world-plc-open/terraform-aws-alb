resource "aws_alb_listener_rule" "listener_rule" {
  listener_arn = var.load_balancer_arn
  priority     = var.priority

  action {
    target_group_arn = var.target_group_arn
    type             = "forward"
  }

  condition {
    field  = var.condition_type
    values = [var.condition_value]
  }
}
