# Change Log

## v1.1.0 (21/12/2021)

* Moved .gitlab-ci.yml with Terraform testing out of the way into test/ as the
  tests are currently broken and need love
* Added .terraform-doc.yml to auto generate (mostly) README.md
* Added basic example config for README.md
* All variables and outputs have descriptions and types, and have been
  organised alphabetically
* Ran `terraform fmt` on the repository
* Updated CONTRIBUTING.md to cover running fmt and ensuring docs are
  regenerated
* Added .gitlab-ci.yml to do basic checks for fmt and out of date docs

## v1.0.0 (28/05/2021)

* Added minimum provider version contraints per Terraform best practice
* Confirmed terraform validate passing for terraform 0.13, 0.14, and 0.15
* Minimum aws provider version set to where support for .aws/sso/cache is enabled
* Ran terraform fmt on repo to start off standardised formatting
* Removed use of map() and list() functions as they're finally depreciated in 0.15

## v0.4.1 (01/12/2021)

* Reverted to Terraform v0.12.X for minimum support as no 0.13.X features in
  use, and provide migration path between v0.3.0 and v1.0.0 of the module

## v0.4.0 (11/11/2020)

* Upgraded to Terraform v.0.13.4

## v0.3.0 (17/04/2020)

* Adds support for supplying a map of tags to the module using the custom_tag arguments.
* Adds unit testing to the module.

## v0.2.0 (23/01/2020)

* Adds flags to allow listeners to be created without a target group.
